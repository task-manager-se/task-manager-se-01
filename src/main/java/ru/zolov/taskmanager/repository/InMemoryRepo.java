package ru.zolov.taskmanager.repository;

import ru.zolov.taskmanager.entity.Project;
import ru.zolov.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class InMemoryRepo {

    public List<Project> getProjects() {
        return storage;
    }

    private List<Project> storage = new ArrayList<Project>();
    private List<Task> tasks = new ArrayList<Task>();

    public InMemoryRepo() {
    }

    public void create(String name) {
        storage.add(new Project(name));
    }

    public void readAll() {
        for (Project p : storage){
            System.out.println(p);
        }
    }

    public void update(int id, String name) {
        for (Project p : storage){
            if(id == p.getProjectId()){
                p.setProjectName(name);
            }
        }
        System.out.println("New project name " + name);
    }

    public void delete(int id) {
        for (Project p : storage){
            if(id == p.getProjectId()){
                storage.remove(p);
            }
        }
    }
    public void createTask(int projId){
        tasks.add(new Task(projId));
    }

    public void readAllTasks(){
        for (Task t : tasks){
            System.out.println(t);
        }
    }
    public void readTaskByProjId(int id){
        for (Task t : tasks){
            if(id == t.getProjectId()){
                System.out.println(t);
            }
        }
    }
    public void readTaskById(int id){
        for (Task t : tasks){
            if(id == t.getTaskId()){
                System.out.println(t);
            }
        }
    }
    public void updateTask(int id, String description){
        for (Task t : tasks){
            if(id == t.getTaskId()){
                t.setDescription(description);
            }
        }
    }

    public void deleteTask(int id){
        for (Task t : tasks){
            if(id == t.getTaskId()){
                tasks.remove(t);
            }
        }
    }
}
