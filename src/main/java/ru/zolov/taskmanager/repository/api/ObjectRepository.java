package ru.zolov.taskmanager.repository.api;

public interface ObjectRepository {


    void create(String name);

    void readAll();

    void update(int id, String name);

    public void delete(int id);

}
