# Project description

Welcome to the home page of the Task Manager project.

## Software requirements

Java 1.6 or higher

## Technology stack
- Java 1.8

- Maven 4

- JUnit 4

  

## Build
  
  ```
mvn clean install
  ```
  
## Run


  ```
  java -jar C:\path\to\folder\task-manager-se-02-1.0-SNAPSHOT.jar
  ```


## Commands

  Check "*help*" for display task manager commands

## Contacts 
**Developers:**

**Igor Zolov** 

- **email**: i.zolov@yandex.ru

  

